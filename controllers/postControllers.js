const Post = require("../models/Post");

exports.getAllPosts = async (req, res, next) => {
  try {
    const [posts, _] = await Post.findAll();
    res.status(200).json({ count: posts.length, posts });
  } catch (error) {
    console.log(error);
    next(error);
  }
};

exports.getPostById = async (req, res, next) => {
  try {
    let postId = Number(req.params.id); // mariaDB tietotyypistä tarkempi.
    let [post, _] = await Post.findById(postId);
    res.status(200).json({ post: post[0] });
  } catch (error) {
    console.log(error);
    next(error);
  }
};

exports.createNewPost = async (req, res, next) => {
  try {
    let { title, body } = req.body;
    let post = new Post(title, body);
    post = await post.save();
    res.status(201).json({ message: "Post created" });
  } catch (error) {
    console.log(error);
    next(error);
  }
};

exports.updatePostById = async (req, res, next) => {
  try {
    let postId = Number(req.params.id);
    let { title, body } = req.body;
    let [post, _] = await Post.updateById(postId, title, body);
    res
      .status(200)
      .json({ message: `message ${req.params.id} was updated`, post: post[0] });
  } catch (error) {
    console.log(error);
    next(error);
  }
};

exports.deletePostById = async (req, res, next) => {
  try {
    let postId = Number(req.params.id);
    let [post, _] = await Post.deleteById(postId);
    res
      .status(200)
      .json({ message: `message ${req.params.id} was deleted`, post: post[0] });
  } catch (error) {
    console.log(error);
    next(error);
  }
};
