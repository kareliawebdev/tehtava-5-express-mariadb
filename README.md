# Tehtava 5 express mariaDB

Luo Node.js REST API jossa käytät express:iä ja MariaDb:tä (tai MySQL:ää).

Lisää koodiisi REST rajapinnan mukaiset metodit: put, delete. Put:lla voit päivittää tietyn id:n perusteella kenttiä title ja body. Delete:llä voit poistaa id:n perusteella tietueen. Noudata lisäämissäsi perusmetodeissa samaa arkkitehtuuria kuin pohjakoodissa on noudatettu (model, controller, reititys).

Lopullisessa REST API:ssa on siis toteutettu metodit get, post, put ja delete.

Testaa sovellustasi Postman:lla että pääset tekemään kaikki operaatiot tietokantaan (tietueiden haku, lisäys, päivitys, poisto). 

Pohjakoodin lähde: [github.com/LloydJanseVanRensburg/Node-Express-starter](https://github.com/LloydJanseVanRensburg/Node-Express-starter)